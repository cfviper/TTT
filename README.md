# TTT - TicTacToe


Based on tutorial by:
https://www.youtube.com/channel/UCyoayn_uVt2I55ZCUuBVRcQ

Working game, up to part 10 of the TicTacToe series. 
https://www.youtube.com/watch?v=7L-4OsuWwNI&list=PLWeGoBm1YHVj7cYQSglzBU0gb7ecDNf4g&index=10


##### Current State:

* Can play as X or O. 
* Only single player.
* Computer player selects random avaliable cells. 


##### TODO:
- Add Music/Sound
- Publish to Play Store
