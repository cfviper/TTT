using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Serialization;
using Random = System.Random;

public class Game : MonoBehaviour {
    public int whoTurn;
    public string isHuman;
    public int turnCount;
    public List<int> computerChoices;
    [FormerlySerializedAs("testVar3")] public int cellIndex;
    
    private bool gameOver;
    public Sprite[] playIcons;

    public Button[] tttSpaces;

    public int[] markedSpaces;
    
    public IDictionary<int, int[]> numberNames = new Dictionary<int, int[]>();

    public Text winnerText;

    public GameObject[] winningLine;

    public GameObject winnerPanel;
    public GameObject switchPlayerPanel;
    public GameObject catPanel;
    public GameObject rematchButton;
    
    // Start is called before the first frame update
    void Start() {
        GameSetup();
    }

    void GameSetup() {
        // Set values for game / reset for new match
        gameOver = false;
        computerChoices = new List<int>(){0,1,2,3,4,5,6,7,8};
        whoTurn = 0;
        turnCount = 0;
        
        foreach (var t in tttSpaces) {
            t.interactable = true;
            t.GetComponent<Image>().sprite = null;
        }

        for (int i = 0; i < markedSpaces.Length; i++) {
            markedSpaces[i] = -100;
        }
        switchPlayerPanel.gameObject.SetActive(true);
        numberNames.Clear();
        
        // ROWS
        numberNames.Add(1,new[]{0,1,2});
        numberNames.Add(2,new[]{3,4,5});
        numberNames.Add(3,new[]{6,7,8});
        // COLUMNS
        numberNames.Add(4,new[]{0,3,6});
        numberNames.Add(5,new[]{1,4,7});
        numberNames.Add(6,new[]{2,5,8});
        // DIAGONALS
        numberNames.Add(7,new[]{0,4,8});
        numberNames.Add(8,new[]{2,4,6});
    }

    public void PlayAsSelection(string selection) {
        switchPlayerPanel.gameObject.SetActive(false);
        isHuman = selection;
        if (isHuman == "O") {
            ComputerPlayer(0);
        }
    }

    // Update is called once per frame
    void Update() { }
    
    private void ComputerPlayer(int whoTurnCmp) {
        whoTurn = whoTurnCmp;
        var random = new Random();
        var randomChoice = random.Next(0,computerChoices.Count);
        cellIndex = computerChoices[randomChoice];
        
        tttSpaces[cellIndex].image.sprite = playIcons[whoTurnCmp];
        tttSpaces[cellIndex].interactable = false;
        markedSpaces[cellIndex] = whoTurnCmp + 1;
        computerChoices.Remove(cellIndex);
        
        turnCount++;
        if (turnCount > 4) {
            WinnerCheck();
        }
    }

    public void TttButton(int whichNumber) {
       
        if (isHuman == "O") {
            whoTurn = 1;
            tttSpaces[whichNumber].image.sprite = playIcons[whoTurn];
            tttSpaces[whichNumber].interactable = false;
            markedSpaces[whichNumber] = whoTurn + 1;
            computerChoices.Remove(whichNumber);

            turnCount++;
            if (turnCount > 4) {
                WinnerCheck();
            }

            if (gameOver == false & computerChoices.Count > 0) {
                ComputerPlayer(0);
            }
        } else if (isHuman == "X") {
            whoTurn = 0;
            tttSpaces[whichNumber].image.sprite = playIcons[whoTurn];
            tttSpaces[whichNumber].interactable = false;
            markedSpaces[whichNumber] = whoTurn + 1;
            computerChoices.Remove(whichNumber);

            turnCount++;
            if (turnCount > 4) {
                WinnerCheck();
            }

            if (gameOver == false & computerChoices.Count>0) {
                ComputerPlayer(1);
            }
        }
        
        
        if (turnCount > 4) {
            WinnerCheck();
        }
        
    }

    private void WinnerCheck() {
        int removal = 0;
        foreach (var sol in numberNames) {
            int[] gridIndex = sol.Value;
            int check = markedSpaces[gridIndex[0]]+ markedSpaces[gridIndex[1]]+ markedSpaces[gridIndex[2]] ;
            
            switch (check) {
                case 3:
                    Debug.Log("WINNER X"+check);
                    WinnerDisplay(sol.Key-1);
                    gameOver = true;
                    break;
                case 6:
                    Debug.Log("WINNER O");
                    WinnerDisplay(sol.Key-1);
                    gameOver = true;
                    break;
            }

            if (check is <= 0 or 3 or 6) continue;
            removal = sol.Key;
        }
        
        if ((turnCount == 9) & (gameOver == false) ) {
            catPanel.gameObject.SetActive(true);
            rematchButton.gameObject.SetActive(true);    
        }

        if (removal > 0) {
            numberNames.Remove(removal);
        }
    }

    private void WinnerDisplay(int indexIn) {
        winnerPanel.gameObject.SetActive(true);
        if (whoTurn == 0) {
            winnerText.text = "Player X Wins!";
        }else if (whoTurn == 1) {
            winnerText.text = "Player O Wins!";
        }

        winningLine[indexIn].SetActive(true);
        rematchButton.gameObject.SetActive(true);
    }

    public void Rematch() {
        GameSetup();
        foreach (var cell in winningLine) {
            cell.gameObject.SetActive(false);
        }
        winnerPanel.gameObject.SetActive(false);
        rematchButton.gameObject.SetActive(false);
        catPanel.gameObject.SetActive(false);
    }
    
}
